var parser = new(require('xmldom').DOMParser)();
var xml = require('../lib/xml-utils');
var select = require('xpath.js');
var requestorFactory = require('oniyi-requestor');

var request = requestorFactory({
  host: 'localhost',
  port: 6379
});

request.get({
    uri: 'https://w3-connections.ibm.com/profiles/atom/connections.do',
    qs: {
      userid: '87f2c6c0-3ae5-1033-85b6-9fda8af4f3da',
      connectionType: 'colleague'
    },
    headers: {
      'user-agent': 'Mozilla/5.0'
    },
    noCache: true
  },
  function(error, response, body) {
    if (error) {
      return console.error(error);
    }
    body = parser.parseFromString(body);
    // var a = xml.find(body, ':link[ref="next"]');
    // console.log(a);
    // console.log(body.getElementsByTagName('link'));
    // console.log(body.getElementsByTagNameNS('http://www.w3.org/2005/Atom', 'link'));

    // var results = select(body, "/*[local-name()='feed' and namespace-uri()='http://www.w3.org/2005/Atom']/*[local-name()='link' and namespace-uri()='http://www.w3.org/2005/Atom']"); // and @*[local-name()='ref' and namespace-uri()='http://www.w3.org/2005/Atom' and .='next']]"));
    var results = select(body, "/*[local-name()='feed' and namespace-uri()='http://www.w3.org/2005/Atom']/*[local-name()='link' and namespace-uri()='http://www.w3.org/2005/Atom']");
    //var results = xml.find(body, 'link');

    Array.prototype.forEach.call(results, function(result){
      console.log(result.tagName + ': ' + result.namespaceURI);
    });
  });