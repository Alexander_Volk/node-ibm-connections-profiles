var profilesService = new(require('../lib/node-ibm-connections-profiles'))({
  endpoint: {
    host: 'w3-connections.ibm.com',
    throttle: {
      disable: false,
      limit: 20,
      duration: 10000
    },
    cache: {
      disable: false,
      storePrivate: true
    }
  },
  defaultRequestOptions: {
    auth: {
      user: 'bkroeger@sg.ibm.com',
      pass: 'vfr432wsx',
      sendImmediately: true
    }
  }
});

var options = {
  targetEmail: 'benjamin.kroeger@de.ibm.com',
  sourceEmail: 'bkroeger@sg.ibm.com',
  tags: ['wwsd', 'test', 'muse', 'socialQ&A', 'socialQA'],
  format: 'full'
};


setTimeout(function() {

  profilesService.updateTags(options)
    .then(function() {
      console.log('updateTags resolved');
      delete options.tags;
      return true;
    })
    .then(function() {
      delete options.sourceEmail;
      return profilesService.getTags(options)
        .then(function(tags) {
          console.log('getTags resolved');
          // console.log(tags);
          return tags;
        });
    })
    .then(function() {
      console.log('removing tag "test"');
      options.tags = ['test'];

      options.sourceEmail = 'bkroeger@sg.ibm.com';
      return profilesService.removeTags(options)
        .then(function() {
          console.log('removeTags resolved');
        });
    })
    .then(function() {
      delete options.sourceEmail;
      return profilesService.getTags(options)
        .then(function(tags) {
          console.log('getTags resolved');
          // console.log(tags);
          return tags;
        });
    })
    .then(function() {
      console.log('adding tag "touchpoint"');
      options.tags = ['touchpoint'];
      options.sourceEmail = 'bkroeger@sg.ibm.com';
      return profilesService.addTags(options)
        .then(function() {
          console.log('addTags resolved');
        });
    })
    .then(function() {
      delete options.sourceEmail;
      return profilesService.getTags(options)
        .then(function(tags) {
          console.log('getTags resolved');
          // console.log(tags);
          return tags;
        });
    }).done(function() {
      console.log('All done');
      console.log('Statistics: %d, %d, %d', profilesService.request.receivedRequests, profilesService.request.cacheMiss, profilesService.request.servedFromCache);
    }, function(reason) {
      console.log('something went wrong');
      console.log(reason);
    }, console.log);
}, 2000);