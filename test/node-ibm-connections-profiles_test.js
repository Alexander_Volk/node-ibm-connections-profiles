/*global describe,it*/
'use strict';
var assert = require('assert'),
  nodeIbmConnectionsProfiles = require('../lib/node-ibm-connections-profiles.js');

describe('node-ibm-connections-profiles node module.', function() {
  it('must be awesome', function() {
    assert( nodeIbmConnectionsProfiles.awesome(), 'awesome');
  });
});
