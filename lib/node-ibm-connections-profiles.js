require('prototypes');

var EventEmitter = require('events').EventEmitter,
  _ = require('lodash'),
  util = require('util'),
  q = require("q"),
  xml = require('./xml-utils'),
  vCardParser = require('oniyi-vcard-parser').factory({
    vCardToJSONAttributeMapping: {
      'UID': 'uid'
    }
  }),
  Requestor = require('oniyi-requestor');

var debug = require('debug'),
  moduleName = 'node-ibm-connections-profiles';

var logError = debug(moduleName + ':error');
// set this namespace to log via console.error
logError.log = console.error.bind(console); // don't forget to bind to console!

var logWarn = debug(moduleName + ':warn');
// set all output to go via console.warn
logWarn.log = console.warn.bind(console);

var logDebug = debug(moduleName + ':debug');
// set all output to go via console.warn
logDebug.log = console.warn.bind(console);

var xmlTemplate = {
    entry: '<entry xmlns="http://www.w3.org/2005/Atom"><category term="profile" scheme="http://www.ibm.com/xmlns/prod/sn/type"></category><content type="text">%s</content></entry>',
    followEntry: '<entry xmlns="http://www.w3.org/2005/Atom"><category term="profiles" scheme="http://www.ibm.com/xmlns/prod/sn/source"></category><category term="profile" scheme="http://www.ibm.com/xmlns/prod/sn/resource-type"></category><category term="%s" scheme="http://www.ibm.com/xmlns/prod/sn/resource-id"></category></entry>',
    makeFriend: '<?xml version="1.0" encoding="UTF-8"?><entry xmlns="http://www.w3.org/2005/Atom" xmlns:snx="http://www.ibm.com/xmlns/prod/sn"><category term="connection" scheme="http://www.ibm.com/xmlns/prod/sn/type" /><category term="colleague" scheme="http://www.ibm.com/xmlns/prod/sn/connection/type" /><category term="pending" scheme="http://www.ibm.com/xmlns/prod/sn/status" /><!-- Message to other user --><content type="html">%s</content></entry>',
    tagsDoc: '<app:categories xmlns:atom="http://www.w3.org/2005/Atom" xmlns:app="http://www.w3.org/2007/app" xmlns:snx="http://www.ibm.com/xmlns/prod/sn"></app:categories>',
    messageEntry: '<entry xmlns="http://www.w3.org/2005/Atom"><content type="text">%s</content><category scheme="http://www.ibm.com/xmlns/prod/sn/type" term="entry" /><category scheme="http://www.ibm.com/xmlns/prod/sn/message-type" term="status" /></entry>'
  },
  profileTagCategories = {
    general: false,
    industries: 'http://www.ibm.com/xmlns/prod/sn/scheme/industries',
    clients: 'http://www.ibm.com/xmlns/prod/sn/scheme/clients',
    skills: 'http://www.ibm.com/xmlns/prod/sn/scheme/skills'
  },
  xmlNS = {
    atom: 'http://www.w3.org/2005/Atom',
    snx: 'http://www.ibm.com/xmlns/prod/sn',
    app: 'http://www.w3.org/2007/app',
    openSearch: 'http://a9.com/-/spec/opensearch/1.1/',
    ibmsc: 'http://www.ibm.com/search/content/2010',
    thr: 'http://purl.org/syndication/thread/1.0',
    fh: 'http://purl.org/syndication/history/1.0'
  },
  requestMethods = {
    get: 'get',
    post: 'post',
    put: 'put',
    head: 'head',
    delete: 'delete'
  };

function htmlEncode(str) {
  return str.replace(/&/g, '&amp;')
    .replace(/"/g, '&quot;')
    .replace(/'/g, '&#39;')
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;');
}

function htmlDecode(str) {
  return str.replace(/&amp;/g, '&')
    .replace(/&quot;/g, '"')
    .replace(/&#39;/g, "'")
    .replace(/&lt;/g, '<')
    .replace(/&gt;/g, '>');
}

function requestRunner(requestMethod, requestOptions, responseBodyParser) {
  var self = this;
  var error;
  var deferred = q.defer();


  try {
    self.request[requestMethod](requestOptions, function(error, response, body, passBackToCache) {
      if (error) {
        if (_.isFunction(passBackToCache)) {
          passBackToCache(error);
        }
        return deferred.reject(error);
      }
      if (response.statusCode < 200 || response.statusCode >= 300) {
        error = new Error(util.format('Failed to execute {%s} request %j', requestMethod, requestOptions));
        error.status = response.statusCode;
        logError(error);
        if (_.isFunction(passBackToCache)) {
          passBackToCache(error);
        }
        return deferred.reject(error);
      }

      // response was coming from cache and is parsed already
      if (response.fromCache && response.processed) {
        return deferred.resolve(JSON.parse(body));
      }

      if (!_.isFunction(responseBodyParser)) {
        return deferred.resolve(body);
      }

      try {
        // response has not been processed yet, so let's do it
        var parsedResponseBody = responseBodyParser(body);

        // if we have had a statusCode === 200, put the parsed response back to cache
        if (requestMethod === 'get' && response.statusCode === 200 && _.isFunction(passBackToCache)) {
          passBackToCache(null, JSON.stringify(parsedResponseBody), 'string');
        }

        // finally resolve the deferred wiht our parsed responseBody
        return deferred.resolve(parsedResponseBody);
      } catch (exception) {
        logError('Failed to parse response from %j', requestOptions);
        logDebug(exception);
        error = new Error(util.format('Failed to parse response from %j', requestOptions));
        error.status = 500;
        if (_.isFunction(passBackToCache)) {
          passBackToCache(error);
        }
        deferred.reject(error);
      }

    });
  } catch (exception) {
    error = new Error(util.format('Failed to execute {%s} request %j', requestMethod, requestOptions));
    error.status = 500;
    logError(error);
    logDebug(exception);
    deferred.reject(error);
  }

  return deferred.promise;
}

var requestValidParameters = [
  'auth',
  'pool',
  'timeout',
  'proxy',
  'strictSSL',
  'agentOptions',
  'jar',
  'tunnel',
  'proxyHeaderWhiteList',
  'proxyHeaderExclusiveList',
  'ttl',
  'disableCache'
];

function extractRequestOptions(defaults, options) {
  var requestOptions = _.merge({}, defaults, _.pick(options, requestValidParameters));

  if (_.isString(options.accessToken)) {
    requestOptions.auth = {
      bearer: options.accessToken
    };
  }
  if (requestOptions.auth && _.isString(requestOptions.auth.bearer)) {
    delete requestOptions.jar;
  }

  return requestOptions;
}

function getAuthPath(requestOptions) {
  if (requestOptions.auth && _.isString(requestOptions.auth.bearer)) {
    return '/oauth';
  }
  return '';
}

function preloadEntriesByUseridBatch(idsArray, cookieJar) {
  var self = this;
  if (_.isArray(idsArray)) {
    idsArray.forEach(function preloadProfile(userid) {
      self.getEntry({
        userid: userid,
        jar: cookieJar
      }).then(null, function(err) {
        logWarn('Failed to get profile {%s} in preload mode', userid);
        logDebug(err);
      });
    });
  }
}

function preloadEntryByEmailBatch(emailsArray, cookieJar) {
  var self = this;
  if (_.isArray(emailsArray)) {
    emailsArray.forEach(function preloadProfile(email) {
      self.getEntry({
        email: email,
        jar: cookieJar
      }).then(null, function(err) {
        logWarn('Failed to get profile {%s} in preload mode', email);
        logDebug(err);
      });
    });
  }
}

// here begins the parser functions definition section

function parseProfileServiceResponse(responseXML) {
  if (_.isString(responseXML)) {
    responseXML = xml.parse(responseXML);
  }

  // @TODO: parse extension attributes and application links
  var result = {
    editableFields: Array.prototype.map.call(responseXML.getElementsByTagNameNS(xmlNS.snx, 'editableField'), function(element) {
      return element.getAttribute('name');
    })
  };

  return result;
}

function parseProfileEntryResponse(responseXML) {
  if (_.isString(responseXML)) {
    responseXML = xml.parse(responseXML);
  }

  // parse vCard String to JSON object
  var entry = vCardParser.toObject((xml.find(responseXML, 'content[type="text"]')[0]).textContent);

  // parsing tags
  if(entry.tags) {
    try {
      entry.tags = entry.tags.split(',');
    } catch (e) {
      logWarn('Failed to parse tags for entry {%s}', entry.userid);
      logDebug(e);
    }
  }

  // also not implemented in xml library yet
  // parse extension attributes
  entry.extattrDetails = {};
  try {
    xml.find(responseXML, 'link[rel="http://www.ibm.com/xmlns/prod/sn/ext-attr"]').forEach(function(val) {
      var extensionId = val.getAttributeNS(xmlNS.snx, 'extensionId');
      entry.extattrDetails[extensionId] = {
        name: extensionId,
        type: val.getAttribute('type'),
        href: val.getAttribute('href'),
        content: entry.extattr[extensionId] || false
      };
    });
  } catch (e) {
    logWarn('Failed to parse extension attributes for entry {%s}', entry.userid);
    logDebug(e);
  }

  return entry;
}

// Network connections handling
function parseNetworkConnectionsResponse(responseXML) {
  responseXML = xml.parse(responseXML);
  var returnValue = {};

  // extract pagination information from received XML
  var paginationLinkElements = xml.select(responseXML, util.format("/*[local-name()='feed' and namespace-uri()='%s']/*[local-name()='link' and namespace-uri()='%s']", xmlNS.atom, xmlNS.atom));
  if (paginationLinkElements.length > 0) {
    returnValue.paginationLinks = {};
    Array.prototype.forEach.call(paginationLinkElements, function(element) {
      returnValue.paginationLinks[element.getAttribute('rel')] = element.getAttribute('href');
    });
  }

  returnValue.totalResults = parseInt(responseXML.getElementsByTagNameNS(xmlNS.openSearch, 'totalResults')[0].textContent, null);
  returnValue.startIndex = parseInt(responseXML.getElementsByTagNameNS(xmlNS.openSearch, 'startIndex')[0].textContent, null);
  returnValue.itemsPerPage = parseInt(responseXML.getElementsByTagNameNS(xmlNS.openSearch, 'itemsPerPage')[0].textContent, null);

  returnValue.networkConnections = {};

  if (_.isString(returnValue.paginationLinks.self) && returnValue.paginationLinks.self.containsIgnoreCase('outputType=profile')) {
    Array.prototype.forEach.call(responseXML.getElementsByTagName('entry'), function(entryXML) {
      var entry = parseProfileEntryResponse(entryXML);
      if (entry && entry.userid) {
        returnValue.networkConnections[entry.userid] = entry;
      }
    });
  } else {
    Array.prototype.forEach.call(responseXML.getElementsByTagName('entry'), function(entry) {
      // could also detect who initialized the connection (author vs. contributor)
      var connection = {
        id: entry.getElementsByTagName('id')[0].textContent.substringFrom('tag:profiles.ibm.com,2006:entry'),
        type: (xml.find(entry, 'category[scheme="http://www.ibm.com/xmlns/prod/sn/type"]')[0]).getAttribute('term'),
        connectionType: (xml.find(entry, 'category[scheme="http://www.ibm.com/xmlns/prod/sn/connection/type"]')[0]).getAttribute('term'),
        status: (xml.find(entry, 'category[scheme="http://www.ibm.com/xmlns/prod/sn/status"]')[0]).getAttribute('term'),
        updated: entry.getElementsByTagName('updated')[0].textContent,
        message: entry.getElementsByTagName('content')[0].textContent,
        summary: entry.getElementsByTagName('summary')[0].textContent,
        links: {
          self: {
            href: (xml.find(entry, 'link[rel="self"]')[0]).getAttribute('href'),
            type: (xml.find(entry, 'link[rel="self"]')[0]).getAttribute('type')
          },
          edit: {
            href: (xml.find(entry, 'link[rel="edit"]')[0]).getAttribute('href'),
            type: (xml.find(entry, 'link[rel="edit"]')[0]).getAttribute('type')
          }
        }
      };
      Array.prototype.forEach.call(entry.getElementsByTagName('contributor'), function(contributor) {
        // have to do this, because xml-utils currently don't support namespaced attribute names
        var rel = contributor.getAttributeNS(xmlNS.snx, 'rel');
        if (_.isString(rel) && rel === 'http://www.ibm.com/xmlns/prod/sn/connection/target') {
          returnValue.networkConnections[contributor.getElementsByTagNameNS(xmlNS.snx, 'userid')[0].textContent] = connection;
          return false;
        }
      });
    });
  }

  return returnValue;
}

// Network connections handling
function parseReportingChainResponse(responseXML) {
  responseXML = xml.parse(responseXML);
  var returnValue = {};

  // extract pagination information from received XML
  var paginationLinkElements = xml.select(responseXML, util.format("/*[local-name()='feed' and namespace-uri()='%s']/*[local-name()='link' and namespace-uri()='%s']", xmlNS.atom, xmlNS.atom));
  if (paginationLinkElements.length > 0) {
    returnValue.paginationLinks = {};
    Array.prototype.forEach.call(paginationLinkElements, function(element) {
      returnValue.paginationLinks[element.getAttribute('rel')] = element.getAttribute('href');
    });
  }

  returnValue.totalResults = parseInt(responseXML.getElementsByTagNameNS(xmlNS.openSearch, 'totalResults')[0].textContent, null);
  returnValue.startIndex = parseInt(responseXML.getElementsByTagNameNS(xmlNS.openSearch, 'startIndex')[0].textContent, null);
  returnValue.itemsPerPage = parseInt(responseXML.getElementsByTagNameNS(xmlNS.openSearch, 'itemsPerPage')[0].textContent, null);

  returnValue.networkConnections = {};

  Array.prototype.forEach.call(responseXML.getElementsByTagName('entry'), function(entryXML){
     var entry = parseProfileEntryResponse(entryXML);
      returnValue.networkConnections[entry.userid] = entry;
  })


  return returnValue;
}

function parseFollowedProfilesResponse(responseXML) {
  if (_.isString(responseXML)) {
    responseXML = xml.parse(responseXML);
  }

  var returnValue = {};

  // extract pagination information from received XML
  var paginationLinkElements = xml.select(responseXML, util.format("/*[local-name()='feed' and namespace-uri()='%s']/*[local-name()='link' and namespace-uri()='%s']", xmlNS.atom, xmlNS.atom));
  if (paginationLinkElements.length > 0) {
    returnValue.paginationLinks = {};
    Array.prototype.forEach.call(paginationLinkElements, function(element) {
      returnValue.paginationLinks[element.getAttribute('rel')] = element.getAttribute('href');
    });
  }

  returnValue.totalResults = parseInt(responseXML.getElementsByTagNameNS(xmlNS.openSearch, 'totalResults')[0].textContent, null);
  returnValue.startIndex = parseInt(responseXML.getElementsByTagNameNS(xmlNS.openSearch, 'startIndex')[0].textContent, null);
  returnValue.itemsPerPage = parseInt(responseXML.getElementsByTagNameNS(xmlNS.openSearch, 'itemsPerPage')[0].textContent, null);

  returnValue.followedProfiles = {};
  Array.prototype.forEach.call(responseXML.getElementsByTagName('entry'), function(followedEntry) {
    var followedResourceId = followedEntry.getElementsByTagName('id')[0].textContent.split('urn:lsid:ibm.com:follow:resource-')[1];
    var userid = xml.find(followedEntry, 'category[scheme="http://www.ibm.com/xmlns/prod/sn/resource-id"]')[0].getAttribute('term');

    returnValue.followedProfiles[userid] = followedResourceId;
  });
  return returnValue;
}

function parseProfileTagsResponse(responseXML) {
  if (_.isString(responseXML)) {
    responseXML = xml.parse(responseXML);
  }

  var categoriesTag = responseXML.getElementsByTagNameNS(xmlNS.app, 'categories')[0];
  var categoryTags = categoriesTag.getElementsByTagNameNS(xmlNS.atom, 'category');

  var returnValue = {
    numberOfContributors: parseInt(categoriesTag.getAttributeNS(xmlNS.snx, 'numberOfContributors'), null),
    contributors: {},
    tags: []
  };

  Array.prototype.forEach.call(categoryTags, function(categoryTag) {
    var contributorTags = categoryTag.getElementsByTagNameNS(xmlNS.atom, 'contributor');
    var tag = {
      term: htmlDecode(categoryTag.getAttribute('term')),
      scheme: categoryTag.getAttribute('scheme'),
      frequency: categoryTag.getAttributeNS(xmlNS.snx, 'frequency'),
      intensityBin: categoryTag.getAttributeNS(xmlNS.snx, 'intensityBin'),
      visibilityBin: categoryTag.getAttributeNS(xmlNS.snx, 'visibilityBin'),
      type: categoryTag.getAttributeNS(xmlNS.snx, 'type'),
      contributors: []
    };

    Array.prototype.forEach.call(contributorTags, function(contributorTag) {
      var contributorGuid = contributorTag.getAttributeNS(xmlNS.snx, 'profileGuid');
      var contributor = returnValue.contributors[contributorGuid] || {
        contribution: {}
      };

      _.merge(contributor, {
        key: contributorTag.getAttributeNS(xmlNS.snx, 'profileKey'),
        userid: contributorGuid,
        uid: contributorTag.getAttributeNS(xmlNS.snx, 'profileUid'),
        email: (contributorTag.getElementsByTagNameNS(xmlNS.atom, 'email')[0]).textContent,
        userState: (contributorTag.getElementsByTagNameNS(xmlNS.snx, 'userState')[0]).textContent,
        isExternal: (contributorTag.getElementsByTagNameNS(xmlNS.snx, 'isExternal')[0]).textContent
      });

      contributor.contribution[tag.type] = contributor.contribution[tag.type] || [];
      contributor.contribution[tag.type].push(tag.term);
      tag.contributors.push(contributor.userid);
      returnValue.contributors[contributorGuid] = contributor;
    });

    returnValue.tags.push(tag);
  });

  return returnValue;
}

// defining the ProfileService
function ProfilesService(options) {
  var self = this;
  if (!options) {
    throw new TypeError('options need to be defined for ProfilesService');
  }
  options = _.merge({
    redis: {
      host: 'localhost',
      port: 6379
    },
    endpoint: {
      schema: 'http',
      host: false,
      contextRoot: '/profiles',
      throttle: {
        disable: false,
        limit: 120,
        duration: 60000
      },
      cache: {
        disable: false,
        storePrivate: false,
        storeNoStore: false,
        ignoreNoLastMod: false,
        requestValidators: [],
        responseValidators: []
      }
    },
    maxProfileAge: 1800,
    defaultRequestOptions: {
      headers: {
        'user-agent': 'Mozilla/5.0'
      }
    }
  }, options);

  if (!options.endpoint.host) {
    throw new TypeError('a hostname for IBM Connections must be specified');
  }
  var requestorOptions = {
    redis: options.redis
  };
  if (!options.endpoint.cache.disable) {
    requestorOptions.cache = {};
    requestorOptions.cache[options.endpoint.host] = options.endpoint.cache;
  } else {
    requestorOptions.disableCache = true;
  }

  if (!options.endpoint.throttle.disable) {
    requestorOptions.throttle = {};
    requestorOptions.throttle[options.endpoint.host] = options.endpoint.throttle;
  }

  _.forOwn(options, function(val, key) {
    self['_' + key] = val;
  });

  self._apiEntryPoint = util.format('%s://%s%s', options.endpoint.schema, options.endpoint.host, options.endpoint.contextRoot);
  self._host = util.format('%s://%s', options.endpoint.schema, options.endpoint.host);

  if (options.requestor) {
    self.request = options.requestor;
    _.forOwn(requestorOptions.throttle, function(config, endpoint) {
      self.request.addLimit(_.merge(_.pick(config, ['limit', 'duration']), {
        endpoint: endpoint
      }));
    });

    _.forOwn(requestorOptions.cache, function(config, endpoint) {
      self.request.addCacheSetting(_.merge(_.pick(config, ['storePrivate', 'storeNoStore', 'ignoreNoLastMod', 'requestValidators', 'responseValidators']), {
        endpoint: endpoint
      }));
    });
  } else {
    self.request = new Requestor(requestorOptions);
  }

  // become an event emitter
  EventEmitter.call(self);
}

util.inherits(ProfilesService, EventEmitter);

/**
 * return profile entry from IBM connections
 * that contains all information from profile
 * @param options
 * @returns {*}
 */
ProfilesService.prototype.getEntry = function(options) {
  var self = this;
  var error;
  return q.promise(function(resolve, reject) {
    var requestOptions = extractRequestOptions(self._defaultRequestOptions, options);

    var authPath = getAuthPath(requestOptions);

    requestOptions.uri = self._apiEntryPoint + authPath + '/atom/profileEntry.do';
    requestOptions.ttl = self._maxProfileAge;

    var qsValidParameters = [
      'email',
      'key',
      'userid'
    ];

    var qs = _.merge({
      format: 'full',
      output: 'vcard'
    }, _.pick(options, qsValidParameters));

    requestOptions.qs = qs;

    var entrySelector = _.pick(qs, 'email', 'key', 'userid');
    if (_.size(entrySelector) !== 1) {
      error = new Error(util.format('Wrong number of entry selectors provided to receive profile entry: %j', entrySelector));
      error.status = 400;
      return reject(error);
    }

    return resolve(requestRunner.call(self, requestMethods.get, requestOptions, parseProfileEntryResponse));
  });
};

/**
 * method wrapper for "getEntry" method
 * @param userid
 * @param cookieJar
 * @returns {*}
 */
ProfilesService.prototype.getEntryByUserid = function(userid, cookieJar) {
  return this.getEntry({
    userid: userid,
    jar: cookieJar
  });
};

/**
 * method wrapper for "getEntry" method
 * @param email
 * @param cookieJar
 * @returns {*}
 */
ProfilesService.prototype.getEntryByEmail = function(email, cookieJar) {
  return this.getEntry({
    email: email,
    jar: cookieJar
  });
};

/**
 * method wrapper for "getEntry" method
 * @param key
 * @param cookieJar
 * @returns {*}
 */
ProfilesService.prototype.getEntryByKey = function(key, cookieJar) {
  return this.getEntry({
    key: key,
    jar: cookieJar
  });
};

/**
 * returns list of users in network
 * @see http://www-10.lotus.com/ldd/lcwiki.nsf/xpAPIViewer.xsp?lookupName=IBM+Connections+5.0+API+Documentation#action=openDocument&res_title=Retrieving_connections_ic50&content=apicontent
 * @param options
 * @returns {*}
 */
ProfilesService.prototype.getNetworkConnections = function(options) {
  var self = this,
    error;

  var promise = q.promise(function(resolve, reject, notify) {
    options = (_.isUndefined(options)) ? {} : options;

    var requestOptions = _.merge({
      ttl: 1800
    }, extractRequestOptions(self._defaultRequestOptions, options));

    var authPath = getAuthPath(requestOptions);

    requestOptions.uri = self._apiEntryPoint + authPath + '/atom/connections.do';

    var qsValidParameters = [
      // 'connectionType',
      'email',
      'key',
      'userid',
      'inclMessage',
      'inclUserStatus',
      'output',
      'outputType',
      'format',
      'page',
      'ps',
      'since',
      'sortBy',
      'sortOrder'
    ];

    var qs = _.merge({
      connectionType: 'colleague',
      outputType: 'connection',
      page: 1
    }, _.pick(options, qsValidParameters));

    var entrySelector = _.pick(qs, 'email', 'key', 'userid');
    if (_.size(entrySelector) !== 1) {
      error = new Error(util.format('Wrong number of entry selectors provided to receive network connections: %j', entrySelector));
      error.status = 400;
      return reject(error);
    }
    if (!_.isString(qs.outputType) || qs.outputType !== 'profile') {
      delete qs.output;
      delete qs.format;
    }
    // output: 'vcard', // 'hcard' this parameter is ignored if "outputType" is not set to "profile"
    if (_.isString(qs.output) && ['hcard', 'vcard'].indexOf(qs.output) < 0) {
      qs.output = 'vcard';
    }
    // format: 'lite',  // 'full'  this parameter is ignored if "outputType" is not set to "profile"
    if (_.isString(qs.format) && ['lite', 'full'].indexOf(qs.format) < 0) {
      qs.format = 'full';
    }
    if (_.isString(qs.sortBy) && ['displayName', 'modified'].indexOf(qs.sortBy) < 0) {
      qs.sortBy = 'displayName';
    }
    if (_.isString(qs.sortOrder) && ['asc', 'desc'].indexOf(qs.sortOrder) < 0) {
      qs.sortOrder = 'asc';
    }
    if (qs.inclMessage && !_.isBoolean(qs.inclMessage)) {
      delete qs.inclMessage;
    }
    if (qs.inclUserStatus && !_.isBoolean(qs.inclUserStatus)) {
      delete qs.inclUserStatus;
    }
    // the connections API does not allow page-sizes larger than 250
    // if fetchAll is set to "true", we increase the page size to maximum
    if (options.fetchAll) {
      qs.page = 1;
      qs.ps = 250;
    } else if (_.isNumber(qs.ps) && qs.ps > 250) {
      qs.ps = 250;
      options.fetchAll = true;
    }

    requestOptions.qs = qs;

    return resolve(requestRunner.call(self, requestMethods.get, requestOptions, parseNetworkConnectionsResponse)
      .then(function(parsedResponse) {
        notify('Received page 1 of resultset');
        // if this was not a call to fetch all the entry's network connections, we're done
        if (!options.fetchAll) {
          return parsedResponse;
        }

        // if it was... but all results fit into a single request, we're don, too
        if (parsedResponse.totalResults === _.size(parsedResponse.networkConnections)) {
          notify('Page 1 contains all available results');
          delete parsedResponse.paginationLinks;
          delete parsedResponse.startIndex;
          delete parsedResponse.itemsPerPage;
          return parsedResponse;
        }

        // we have to request subsequent result pages in order to fetch a complete list of the entry's network connections
        var page1Deferred = q.defer();
        var promiseArray = [page1Deferred.promise];
        page1Deferred.resolve(parsedResponse);

        // run one subsequent request for each page of the result set. Instead of using the paginationLinks,
        // we simply overwrite the "page" parameter of our request's query object and execute all the requests in parallel
        // collecting all request promises in an arry
        for (var i = 2; i <= Math.ceil(parsedResponse.totalResults / requestOptions.qs.ps); i++) {
          var pageRequestOptions = _.clone(requestOptions);
          var pageQs = _.merge(_.clone(requestOptions.qs), {
            page: i
          });
          pageRequestOptions.qs = pageQs;
          var pagePromise = requestRunner.call(self, requestMethods.get, pageRequestOptions, parseNetworkConnectionsResponse);
          pagePromise.then(function() {
            notify(util.format('Received page %d of resultset', i));
          });
          promiseArray.push(pagePromise);
        }

        return q.all(promiseArray).then(function(results) {
          notify(util.format('Resolved all %d pages', results.length));
          var result = _.merge.apply(null, results);

          delete result.paginationLinks;
          delete result.startIndex;
          delete result.itemsPerPage;

          return result;
        });
      }));
  });

  promise.then(function(result) {
    preloadEntriesByUseridBatch.call(self, _.keys(result.networkConnections), options.jar);
  });

  return promise;
};

ProfilesService.prototype.getReportingChain = function(options) {
  var self = this,
    error;

  var promise = q.promise(function(resolve, reject, notify) {
    options = (_.isUndefined(options)) ? {} : options;

    var requestOptions = _.merge({
      ttl: 1800
    }, extractRequestOptions(self._defaultRequestOptions, options));

    var authPath = getAuthPath(requestOptions);

    requestOptions.uri = self._apiEntryPoint + authPath + '/atom/reportingChain.do';

    var qsValidParameters = [
      // 'connectionType',
      'email',
      'key',
      'userid',
      'inclUserStatus',
      'output',
      'format'
    ];

    var qs = _.pick(options, qsValidParameters);

    var entrySelector = _.pick(qs, 'email', 'key', 'userid');
    if (_.size(entrySelector) !== 1) {
      error = new Error(util.format('Wrong number of entry selectors provided to receive network connections: %j', entrySelector));
      error.status = 400;
      return reject(error);
    }

    qs.output = 'vcard';
    qs.format = 'full';

    if (_.isString(qs.format) && ['lite', 'full'].indexOf(qs.format) < 0) {
      qs.format = 'lite';
    }

    if (qs.inclUserStatus && !_.isBoolean(qs.inclUserStatus)) {
      delete qs.inclUserStatus;
    }
    // the connections API does not allow page-sizes larger than 250
    // if fetchAll is set to "true", we increase the page size to maximum
    if (options.fetchAll) {
      qs.page = 1;
      qs.ps = 250;
    } else if (_.isNumber(qs.ps) && qs.ps > 250) {
      qs.ps = 250;
      options.fetchAll = true;
    }

    requestOptions.qs = qs;

    return resolve(requestRunner.call(self, requestMethods.get, requestOptions, parseReportingChainResponse)
      .then(function(parsedResponse) {
        notify('Received page 1 of resultset');
        // if this was not a call to fetch all the entry's network connections, we're done
        if (!options.fetchAll) {
          return parsedResponse;
        }

        // if it was... but all results fit into a single request, we're don, too
        if (parsedResponse.totalResults === _.size(parsedResponse.networkConnections)) {
          notify('Page 1 contains all available results');
          delete parsedResponse.paginationLinks;
          delete parsedResponse.startIndex;
          delete parsedResponse.itemsPerPage;
          return parsedResponse;
        }

        //return parsedResponse;

        // we have to request subsequent result pages in order to fetch a complete list of the entry's network connections
        var page1Deferred = q.defer();
        var promiseArray = [page1Deferred.promise];
        page1Deferred.resolve(parsedResponse);

        // run one subsequent request for each page of the result set. Instead of using the paginationLinks,
        // we simply overwrite the "page" parameter of our request's query object and execute all the requests in parallel
        // collecting all request promises in an arry
        for (var i = 2; i <= Math.ceil(parsedResponse.totalResults / requestOptions.qs.ps); i++) {
          var pageRequestOptions = _.clone(requestOptions);
          var pageQs = _.merge(_.clone(requestOptions.qs), {
            page: i
          });
          pageRequestOptions.qs = pageQs;
          var pagePromise = requestRunner.call(self, requestMethods.get, pageRequestOptions, parseReportingChainResponse);
          pagePromise.then(function() {
            notify(util.format('Received page %d of resultset', i));
          });
          promiseArray.push(pagePromise);
        }

        return q.all(promiseArray).then(function(results) {
          notify(util.format('Resolved all %d pages', results.length));
          var result = _.merge.apply(null, results);

          delete result.paginationLinks;
          delete result.startIndex;
          delete result.itemsPerPage;

          return result;
        });
      }));
  });

  promise.then(function(result) {
    console.log('promise then');
    preloadEntriesByUseridBatch.call(self, _.keys(result.networkConnections), options.jar);
  });

  return promise;
};

//TODO: need to test
ProfilesService.prototype.getFollowedProfiles = function(options) {
  var self = this;

  var promise = q.promise(function(resolve, reject, notify) {
    options = (_.isUndefined(options)) ? {} : options;

    var requestOptions = _.merge(extractRequestOptions(self._defaultRequestOptions, options), {
      headers: {
        accept: 'application/xml'
      }
    });

    var authPath = getAuthPath(requestOptions);

    requestOptions.uri = self._apiEntryPoint + '/follow' + authPath + '/atom/resources';

    var qsValidParameters = [
      'page',
      'ps',
      'resource'
    ];

    var qs = _.merge({
      type: 'profile',
      source: 'profiles',
      page: 1
    }, _.pick(options, qsValidParameters));

    // the connections API does not allow page-sizes larger than 20
    // if fetchAll is set to "true", we increase the page size to maximum
    if (options.fetchAll) {
      qs.page = 1;
      qs.ps = 20;
    } else if (_.isNumber(qs.ps) && qs.ps > 20) {
      qs.ps = 20;
      options.fetchAll = true;
    }

    requestOptions.qs = qs;

    return resolve(requestRunner.call(self, requestMethods.get, requestOptions, parseFollowedProfilesResponse)
      .then(function(parsedResponse) {
        notify('Received page 1 of resultset');
        // if this was not a call to fetch all the authenticated person's followed profiles, we're done
        if (!options.fetchAll) {
          return resolve(parsedResponse);
        }

        // if it was... but all results fit into a single request, we're done, too
        if (parsedResponse.totalResults === _.size(parsedResponse.followedProfiles)) {
          notify('Page 1 contains all available results');
          delete parsedResponse.paginationLinks;
          delete parsedResponse.startIndex;
          delete parsedResponse.itemsPerPage;
          return resolve(parsedResponse);
        }

        // we have to request subsequent result pages in order to fetch a complete list of followed profiles
        var page1Deferred = q.defer();
        var promiseArray = [page1Deferred.promise];
        page1Deferred.resolve(parsedResponse);

        // run one subsequent request for each page of the result set. Instead of using the paginationLinks,
        // we simply overwrite the "page" parameter of our request's query object and execute all the requests in parallel
        // collecting all request promises in an arry
        for (var i = 2; i <= Math.ceil(parsedResponse.totalResults / requestOptions.qs.ps); i++) {
          var pageRequestOptions = _.clone(requestOptions);
          var pageQs = _.merge(_.clone(requestOptions.qs), {
            page: i
          });
          pageRequestOptions.qs = pageQs;

          var pagePromise = requestRunner.call(self, requestMethods.get, pageRequestOptions, parseFollowedProfilesResponse);

          pagePromise.then(function() {
            notify(util.format('Received page %d of resultset', i));
          });
          promiseArray.push(pagePromise);
        }

        return q.all(promiseArray).then(function(results) {
          notify(util.format('Resolved all %d pages', results.length));
          var result = _.merge.apply(null, results);

          delete result.paginationLinks;
          delete result.startIndex;
          delete result.itemsPerPage;

          return result;
        });
      }));
  });

  promise.then(function(result) {
    preloadEntriesByUseridBatch.call(self, _.keys(result.followedProfiles), options.jar);
  });
  return promise;
};

ProfilesService.prototype.getEditableFields = function(options) {
  var self = this;
  var error;

  return q.promise(function(resolve, reject) {
    var qsValidParameters = [
      'email',
      'userid'
    ];

    var qs = _.pick(options, qsValidParameters);

    var entrySelector = _.pick(qs, 'email', 'userid');
    if (_.size(entrySelector) !== 1) {
      error = new Error(util.format('Wrong number of entry selectors provided to receive editable fields: %j', entrySelector));
      error.status = 400;
      return reject(error);
    }
    var requestOptions = _.merge(extractRequestOptions(self._defaultRequestOptions, options), {
      headers: {
        accept: 'application/xml'
      },
      disableCache: true
    });

    var authPath = getAuthPath(requestOptions);

    requestOptions.uri = self._apiEntryPoint + authPath + '/atom/profileService.do';
    requestOptions.qs = qs;

    return resolve(requestRunner.call(self, requestMethods.get, requestOptions, parseProfileServiceResponse));
  });
};

// TODO need to test
ProfilesService.prototype.updateEntry = function(options) {
  var self = this;
  var error;
  return q.promise(function(resolve, reject, notify) {
    var entry = options.entry;
    if (!entry || !entry.key) {
      error = new Error(util.format('A valid entry must be provided to update it %j', entry));
      error.status = 400;
      logError(error);
      logDebug(options);
      return reject(error);
    }
    return resolve(self.getEditableFields(options).then(function(editableFields) {
      notify('Received list of editableFields');
      var requestOptions = _.merge(extractRequestOptions(self._defaultRequestOptions, options), {
        qs: {
          key: entry.key
        },
        body: util.format(xmlTemplate.entry, vCardParser.toVcard(entry)),
        headers: {
          accept: 'application/atom+xml'
        }
      });

      var authPath = getAuthPath(requestOptions);

      requestOptions.uri = self._apiEntryPoint + authPath + '/atom/entry.do';

      if (editableFields.indexOf('jobResp') > -1 && entry.jobResp && entry.jobResp.length > 128) {
        entry.jobResp = entry.jobResp.substr(0, 127);
      }

      return requestRunner.call(self, requestMethods.put, requestOptions).then(function() {
        notify('Updated profile entry successfully');
        var promisesArray = [];
        _.forOwn(entry.extextattrDetailsAttr, function(extAttr) {
          if (editableFields.indexOf(extAttr.name) > -1) {
            var extAttrRequestOptions = _.omit(_.clone(requestOptions), ['qs', 'body', 'method']);

            extAttrRequestOptions.uri = self._apiEntryPoint + authPath + extAttr.href.substringFrom(self._endpoint.host + self._endpoint.contextRoot);

            var requestMethod = (entry.extattr[extAttr.name]) ? requestMethods.put : requestMethods.delete;

            if (requestMethod === 'put') {
              extAttrRequestOptions.body = decodeURIComponent(entry.extattr[extAttr.name]);
              _.merge(extAttrRequestOptions.headers, {
                'Content-type': extAttr.type
              });
            }
            var extAttrPromise = requestRunner.call(self, requestMethod, extAttrRequestOptions);

            extAttrPromise.then(function() {
              notify(util.format('Successfully executed {%s} request for extensionAttribute {%s}', requestMethod, extAttr.name));
            });

            promisesArray.push(extAttrPromise);
          }
        });
        return q.all(promisesArray);
      });
    }));
  });
};

/**
 * returns list of tags of specified user
 *
 * @see http://www-10.lotus.com/ldd/lcwiki.nsf/xpAPIViewer.xsp?lookupName=IBM+Connections+5.0+API+Documentation#action=openDocument&res_title=Retrieving_profile_tags_ic50&content=apicontent
 * @param options
 * @returns {*}
 */
ProfilesService.prototype.getTags = function(options) {
  var self = this,
    error;

  var promise = q.promise(function(resolve, reject) {
    var requestOptions = _.merge({
      ttl: 1800
    }, extractRequestOptions(self._defaultRequestOptions, options), {
      headers: {
        accept: 'application/xml'
      }
    });

    var authPath = getAuthPath(requestOptions);

    requestOptions.uri = self._apiEntryPoint + authPath + '/atom/profileTags.do';

    var qsValidParameters = [
      'targetEmail',
      'targetKey',
      'sourceEmail',
      'sourceKey',
      'format',
      'lastMod'
    ];

    var qs = _.pick(options, qsValidParameters);

    var targetSelector = _.pick(qs, 'targetEmail', 'targetKey');
    if (_.size(targetSelector) !== 1) {
      error = new Error(util.format('Wrong number of targetEntry selectors provided to receive tags: %j', targetSelector));
      error.status = 400;
      return reject(error);
    }
    var sourceSelector = _.pick(qs, 'sourceEmail', 'sourceKey');
    if (_.size(sourceSelector) > 1) {
      error = new Error(util.format('Wrong number of sourceEntry selectors provided to receive tags: %j', sourceSelector));
      error.status = 400;
      return reject(error);
    }

    // format will be ignores on server if a valid sourceSelector was provided
    if (_.isString(qs.format) && ['lite', 'full'].indexOf(qs.format) < 0) {
      qs.format = 'lite';
    }

    requestOptions.qs = qs;

    return resolve(requestRunner.call(self, requestMethods.get, requestOptions, parseProfileTagsResponse));
  });

  promise.then(function(result) {
    preloadEntriesByUseridBatch.call(self, _.keys(result.contributors), options.jar);
  });
  return promise;
};


/**
 * method wrapper of updateTags
 * insteed of fully replace tags as "updateTags" allows to specify only
 * new tags, that need to add
 *
 * @see updateTags method
 * @param options
 * @returns {*}
 */
ProfilesService.prototype.addTags = function(options) {
  var self = this,
    error;
  return q.promise(function(resolve, reject, notify) {

    if (!_.isArray(options.tags)) {
      error = new Error('an Array of tags must be provided');
      error.status = 400;
      return reject(error);
    }

    var qsValidParameters = [
      'targetEmail',
      'targetKey',
      'sourceEmail',
      'sourceKey',
      'lastMod'
    ];

    var requestOptions = _.merge(extractRequestOptions(self._defaultRequestOptions, options), _.pick(options, qsValidParameters), {
      disableCache: true
    });

    return resolve(self.getTags(requestOptions)
      .then(function(parsedProfileTags) {
        notify('Received existing tags');
        var existingTags = parsedProfileTags.tags.map(function(tag) {
          return _.pick(tag, 'term', 'type');
        });
        options.tags = options.tags.map(function(tag) {
          if (_.isString(tag)) {
            tag = {
              term: tag
            };
          }
          return tag;
        });
        requestOptions.tags = _.union(options.tags, existingTags);
        return self.updateTags(requestOptions);
      }));
  });
};

/**
 * method wrapper of updateTags
 * insteed of fully replace tags as "updateTags" allows to specify only
 * thoose tags, that need to remove
 *
 * @see updateTags method
 * @param options
 * @returns {*}
 */
ProfilesService.prototype.removeTags = function(options) {
  var self = this,
    error;
  return q.promise(function(resolve, reject, notify) {

    if (!_.isArray(options.tags)) {
      error = new Error('an Array of tags must be provided');
      error.status = 400;
      return reject(error);
    }

    var qsValidParameters = [
      'targetEmail',
      'targetKey',
      'sourceEmail',
      'sourceKey',
      'lastMod'
    ];

    var requestOptions = _.merge(extractRequestOptions(self._defaultRequestOptions, options), _.pick(options, qsValidParameters), {
      disableCache: true
    });

    return resolve(self.getTags(requestOptions)
      .then(function(parsedProfileTags) {
        notify('Received existing tags');
        var existingTags = parsedProfileTags.tags.map(function(tag) {
          return JSON.stringify(_.pick(tag, 'term', 'type'));
        });
        options.tags = options.tags.map(function(tag) {
          if (_.isString(tag)) {
            tag = {
              term: tag
            };
          }
          if (!_.isString(tag.type)) {
            tag.type = 'general';
          }
          return JSON.stringify(tag);
        });

        requestOptions.tags = _.difference(existingTags, options.tags).map(function(tag) {
          return JSON.parse(tag);
        });
        return self.updateTags(requestOptions);
      }));
  });
};

/**
 * method that provides a possibility remove tag
 * @param options
 * @returns {*}
 */
ProfilesService.prototype.removeTag = function(options) {
  var self = this,
    error;
  return q.promise(function(resolve, reject, notify) {

    var requestOptions = _.merge(extractRequestOptions(self._defaultRequestOptions, options), {
      headers: {
        'Content-Type': 'application/atom+xml'
      }
    });

    var qsValidParameters = [
      'targetEmail',
      'targetKey',
      'tag'
    ];

    var authPath = getAuthPath(requestOptions);

    requestOptions.uri = self._apiEntryPoint + authPath + '/atom/profileTags.do';

    requestOptions.qs = _.pick(options, qsValidParameters);
    return resolve(requestRunner.call(self, requestMethods.delete, requestOptions));
  });
};

/**
 * update tag list for specified user
 * replaces old list of tags with new
 *
 * @see http://www-10.lotus.com/ldd/lcwiki.nsf/xpAPIViewer.xsp?lookupName=IBM+Connections+5.0+API+Documentation#action=openDocument&res_title=Updating_profile_tags_ic50&content=apicontent
 * @see addTags and removeTags methods
 * @param options
 * @returns {*}
 */
ProfilesService.prototype.updateTags = function(options) {
  var self = this,
    error;

  return q.promise(function(resolve, reject) {
    if (!_.isArray(options.tags)) {
      error = new Error('an Array of tags must be provided');
      error.status = 400;
      return reject(error);
    }
    var requestOptions = _.merge(extractRequestOptions(self._defaultRequestOptions, options), {
      headers: {
        'Content-Type': 'application/atom+xml'
      }
    });

    var authPath = getAuthPath(requestOptions);

    requestOptions.uri = self._apiEntryPoint + authPath + '/atom/profileTags.do';

    var qsValidParameters = [
      'targetEmail',
      'targetKey',
      'sourceEmail',
      'sourceKey',
      'lastMod'
    ];

    var qs = _.pick(options, qsValidParameters);

    var targetSelector = _.pick(qs, 'targetEmail', 'targetKey');
    if (_.size(targetSelector) !== 1) {
      error = new Error(util.format('Wrong number of targetEntry selectors provided to update tags: %j', targetSelector));
      error.status = 400;
      return reject(error);
    }
    var sourceSelector = _.pick(qs, 'sourceEmail', 'sourceKey');
    if (_.size(sourceSelector) !== 1) {
      error = new Error(util.format('Wrong number of sourceEntry selectors provided to update tags: %j', sourceSelector));
      error.status = 400;
      return reject(error);
    }

    requestOptions.qs = qs;

    var tagsDoc = xml.parse(xmlTemplate.tagsDoc),
      tagsDocParentNode = tagsDoc.getElementsByTagNameNS(xmlNS.app, 'categories')[0];

    options.tags.forEach(function(tag) {
      var xmlTag = tagsDoc.createElement('atom:category');
      if (_.isString(tag)) {
        tag = {
          term: tag
        };
      }
      xmlTag.setAttribute('term', htmlEncode(tag.term));
      if (_.isString(profileTagCategories[tag.category])) {
        xmlTag.setAttribute('scheme', profileTagCategories[tag.category]);
      }
      tagsDocParentNode.appendChild(xmlTag);
    });

    // this might require the XMLSerializer.serializeToString(tagsDoc) from xmldom package
    requestOptions.body = tagsDoc.toString();
    return resolve(requestRunner.call(self, requestMethods.put, requestOptions));
  });
};


/**
 * tag typeahead
  * @see http://www-10.lotus.com/ldd/lcwiki.nsf/xpAPIViewer.xsp?lookupName=IBM+Connections+5.0+API+Documentation#action=openDocument&res_title=Tag_Type_Ahead_API_ic50&content=apicontent&sa=true
 * @param options
 * @returns {*}
 */
// TODO these still need to be re-worked
ProfilesService.prototype.tagTypeahead = function(options) {
  var self = this;

  var deferred = q.defer();
  var promise = q.promise(function(resolve, reject) {
    var requestOptions = _.merge(extractRequestOptions(self._defaultRequestOptions, options));

    var authPath = getAuthPath(requestOptions);

    requestOptions.uri = self._host + '/search/json/mytag';

    requestOptions.qs = {
      tag: options.term,
    }

    return resolve(requestRunner.call(self, requestMethods.get, requestOptions));
  });

  promise.then(
    function(data){
      deferred.resolve(JSON.parse(data));
    },
    function(error){
      deferred.reject(error);
    })

  return deferred.promise;
}

/**
 * search users according documentation
 * @see http://www-10.lotus.com/ldd/lcwiki.nsf/xpAPIViewer.xsp?lookupName=IBM+Connections+5.0+API+Documentation#action=openDocument&res_title=People_Finder_API&content=apicontent
 * @param options
 * @returns {*}
 */
ProfilesService.prototype.searchPeople = function(options) {
  var self = this;

  var deferred = q.defer();
  var promise = q.promise(function(resolve, reject) {
    var requestOptions = _.merge(extractRequestOptions(self._defaultRequestOptions, options));

    var authPath = getAuthPath(requestOptions);

    requestOptions.uri = self._host + '/search/basic/people/typeahead';

    requestOptions.qs = {
      query: options.term,
      pageSize: options.pageSize,
      additionalFields: {'low':['country','phoneNumber'],'high':['tag']},
      searchOnlyNameAndEmail: false,
      mustMatchNameOrEmail: false,
      highlight: false
    }

    return resolve(requestRunner.call(self, requestMethods.get, requestOptions));
  });

  promise.then(
    function(data){
      deferred.resolve(JSON.parse(data));
    },
    function(error){
      deferred.reject(error);
    })

  return deferred.promise;
}


// TODO: need to test it
ProfilesService.prototype.getNetworkState = function(options) {
  var self = this;
  var error;

  var promiseNetworkState = q.promise(function(resolve, reject) {
    var requestOptions = _.merge(extractRequestOptions(self._defaultRequestOptions, options), {
      headers: {
        'accept': 'application/atom+xm',
        'Content-Type': 'application/atom+xml'
      }
    });

    var authPath = getAuthPath(requestOptions);

    requestOptions.uri = self._apiEntryPoint + authPath + '/atom/connections.do';

    requestOptions.qs = {
      connectionType: 'colleague',
      sourceUserid: options.userid,
      targetUserid: options.targetid
    };

    return resolve(requestRunner.call(self, requestMethods.get, requestOptions));
  });

  var deferred = q.defer();

  promiseNetworkState.then(function(data){
    var body = xml.parse(data);

    var result = null;
    var entryList = body.getElementsByTagName('entry');
    if (entryList.length > 0) {
      var entryNode = entryList[0];
      result = {
      connectionId: (entryNode.getElementsByTagName('id')[0]).textContent.substringFrom('tag:profiles.ibm.com,2006:entry'),
      status: (entryNode.getElementsByTagName('category')[2]).getAttribute('term')
      }
    }
    deferred.resolve(result);
  }, function(a){
    deferred.reject(null);
  })

  return deferred.promise;
}

/**
 * sends an invitation to add to network
 * @see http://www-10.lotus.com/ldd/lcwiki.nsf/xpAPIViewer.xsp?lookupName=IBM+Connections+5.0+API+Documentation#action=openDocument&res_title=Inviting_a_person_to_become_your_colleague_ic50&content=apicontent
 * @param options
 * @returns {*}
 */
ProfilesService.prototype.inviteContact = function (options) {
  var self = this;
  var error;

  return q.promise(function(resolve, reject) {
    var requestOptions = _.merge({
      ttl: 1800
    }, extractRequestOptions(self._defaultRequestOptions, options), {
      headers: {
        accept: 'application/xml'
      }
    });

    var authPath = getAuthPath(requestOptions);

    requestOptions.uri = self._apiEntryPoint + authPath + '/atom/connections.do';

    requestOptions.qs = {
      connectionType: 'colleague',
      userid: options.targetid
    };

    requestOptions.body = util.format(xmlTemplate.makeFriend, options.message);

    return resolve(requestRunner.call(self, requestMethods.post, requestOptions));
  });
}

/**
 * removes specified user from current user's network
 * @param options
 * @returns {*}
 */
ProfilesService.prototype.removeFromNetwork = function (options) {
  var self = this;
  var deferred = q.defer();

  self.getNetworkState(options)
    .then(function(connection) {
      if (!connection.connectionId) {
        return deferred.resolve();
      }

      var promise = q.promise(function (resolve, reject) {
        var requestOptions = _.merge(extractRequestOptions(self._defaultRequestOptions, options), {
          headers: {
            accept: 'application/xml'
          }
        });

        var authPath = getAuthPath(requestOptions);

        requestOptions.uri = self._apiEntryPoint + authPath + '/atom/connection.do?connectionId=' + connection.connectionId + '&inclMessage=true';

        return resolve(requestRunner.call(self, requestMethods.delete, requestOptions));
      });

      promise.then(
        function(data){
          return deferred.resolve(data);
        },
        function(error){
          return deferred.reject(error);
        })
    }, function(error){
      return deferred.reject(error);
    });
  return deferred.promise;
}

/**
 * if specified user is following by current user -
 * return following resource id (this id need to unfollow)
 * in other case - returns false
 * @see http://www-10.lotus.com/ldd/lcwiki.nsf/xpAPIViewer.xsp?lookupName=IBM+Connections+5.0+API+Documentation#action=openDocument&res_title=Getting_a_feed_of_the_followed_resources_ic50&content=apicontent
 * @param options = {targetid: 'specified user's id'}
 * @returns {*}
 */
ProfilesService.prototype.getFollowState = function (options) {
  var self = this;
  var promise = q.promise(function(resolve, reject) {
    var requestOptions = _.merge({
      ttl: 1800
    }, extractRequestOptions(self._defaultRequestOptions, options), {
      headers: {
        accept: 'application/xml'
      }
    });

    var authPath = getAuthPath(requestOptions);

    requestOptions.uri = self._apiEntryPoint + authPath + '/follow/atom/resources';

    requestOptions.qs = {
      source: 'PROFILES',
      type: 'PROFILE',
      resource: options.targetid
    }

    return resolve(requestRunner.call(self, requestMethods.get, requestOptions));
  });

  var deferred = q.defer();

  promise.then(function(data){
    var body = xml.parse(data);

    var result = false;
    if (body.getElementsByTagName('entry').length === 1) {
      var entryNode = body.getElementsByTagName('entry')[0];
      result = (entryNode.getElementsByTagName('id')[0]).textContent.substringFrom('urn:lsid:ibm.com:follow:resource-');
      //result = true;
    }
    deferred.resolve(result);
  }, function(a){
    deferred.reject(a);
  })

  return deferred.promise;
}

/**
 * adds new message to specified user's message board
 * to add message to own message board - need set userid options to '@me'
 * @see http://www-10.lotus.com/ldd/lcwiki.nsf/xpAPIViewer.xsp?lookupName=IBM+Connections+5.0+API+Documentation#action=openDocument&res_title=Posting_microblog_entries_ic50&content=apicontent
 * @param options = {userid: 'specified user's id'} || {userid: '@me'}
 * @returns {*}
 */
ProfilesService.prototype.createMessage = function (options) {
  var self = this;
  return q.promise(function(resolve, reject) {
    var requestOptions = _.merge(extractRequestOptions(self._defaultRequestOptions, options), {
      headers: {
        accept: 'application/json',
        'Content-Type': 'application/json'
      }
    });

    var authPath = getAuthPath(requestOptions);

    requestOptions.uri = self._host + '/connections/opensocial/basic/rest/ublog/'+options.userid+'/@all';

    requestOptions.qs = {
      key: options.userid,
    }

    requestOptions.body = JSON.stringify({"content": options.message});

    return resolve(requestRunner.call(self, requestMethods.post, requestOptions));
  });
}

/**
 * start following specified user
 * @param options
 * @returns {*}
 */
ProfilesService.prototype.follow = function (options) {
  var self = this;
  var error;
  return q.promise(function(resolve, reject) {
    var requestOptions = _.merge({
      ttl: 1800
    }, extractRequestOptions(self._defaultRequestOptions, options), {
      headers: {
        accept: 'application/xml'
      }
    });

    var authPath = getAuthPath(requestOptions);

    requestOptions.uri = self._apiEntryPoint + authPath + '/follow/atom/resources';

    requestOptions.qs = {
      source: 'profiles',
      type: 'profile'
    }

    requestOptions.body = util.format(xmlTemplate.followEntry, options.targetid);

    return resolve(requestRunner.call(self, requestMethods.post, requestOptions));
  });
}

/**
 * stops following specified user
 * @param options
 * @returns {*}
 */
ProfilesService.prototype.unfollow = function (options) {
  var self = this;
  var deferred = q.defer();

  self.getFollowState(options)
    .then(function(followedResourceId) {
      if (!followedResourceId) {
        return deferred.resolve();
      }

      var promise = q.promise(function (resolve, reject) {
        var requestOptions = _.merge({
          ttl: 1800
        }, extractRequestOptions(self._defaultRequestOptions, options), {
          headers: {
            accept: 'application/xml'
          }
        });

        var authPath = getAuthPath(requestOptions);

        requestOptions.uri = self._apiEntryPoint + authPath + '/follow/atom/resources/' + followedResourceId;

        requestOptions.qs = {
          source: 'profiles',
          type: 'profile',
          resource: options.targetid
        }

        return resolve(requestRunner.call(self, requestMethods.delete, requestOptions));
      });

      promise.then(
      function(data){
        return deferred.resolve();
      },
      function(){
        return deferred.reject();
      })
    });
  return deferred.promise;
}



/***********************************/
/*              old                */
/***********************************/


ProfilesService.prototype.nameTypeahead = function(options) {
  var self = this,
    error;

  return q.promise(function(resolve, reject) {
    var requestOptions = _.merge({
      ttl: 1800
    }, extractRequestOptions(self._defaultRequestOptions, options), {
      requestValidators: [
        function(requestOptions, evaluator) {
          evaluator.flagStorable(true);
          evaluator.flagRetrievable(true);
          return true;
        }
      ]
    });

    requestOptions.uri = self._apiEntryPoint + '/html/nameTypeahead.do';

    var qsValidParameters = [
      'count',
      'name'
    ];

    var qs = _.merge({
      count: 20
    }, _.pick(options, qsValidParameters), {
      lang: 'en_us',
      extended: true
    });

    if (!_.isString(qs.name) && qs.name.length > 0) {
      error = new Error(util.format('options.name must be a string %j', qs));
      error.status = 400;
      return reject(error);
    }

    requestOptions.qs = qs;

    return resolve(requestRunner.call(self, requestMethods.get, requestOptions, function(data) {
      // default response will contain information that prevents JSON.parse from completing successfully
      data = data.trim().substringFrom('/*').substringUpTo('*/');

      return JSON.parse(data);
    }));
  });
};


exports = module.exports = ProfilesService;
