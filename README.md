#  [![Build Status](https://secure.travis-ci.org/benkroeger/node-ibm-connections-profiles.png?branch=master)](http://travis-ci.org/benkroeger/node-ibm-connections-profiles)

> an implementation of convenience wrappers for the IBM Connections Profiles API


## Getting Started

Install the module with: `npm install node-ibm-connections-profiles`

```js
var node-ibm-connections-profiles = require('node-ibm-connections-profiles');
node-ibm-connections-profiles.awesome(); // "awesome"
```

Install with cli command


## Documentation

_(Coming soon)_


## Examples

_(Coming soon)_


## Contributing

In lieu of a formal styleguide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality. Lint and test your code using [Grunt](http://gruntjs.com).


## License

Copyright (c) 2014 Benjamin Kroeger
Licensed under the MIT license.
